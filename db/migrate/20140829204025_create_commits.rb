class CreateCommits < ActiveRecord::Migration
  def change
    create_table :commits do |t|
      t.integer :post_id
      t.text :body

      t.timestamps
    end
  end
end
