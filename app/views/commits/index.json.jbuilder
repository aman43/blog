json.array!(@commits) do |commit|
  json.extract! commit, :id, :post_id, :body
  json.url commit_url(commit, format: :json)
end
